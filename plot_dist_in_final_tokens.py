
from AcoModels import AcoModel
from AcoModels import AcoModelSet
from ArkReader import ArkReader

import matplotlib.pyplot as plt


class PlotDistInFinalTokens:
    """ """

    def load(self, filename, ftr_vector):
        """ """

        words = dict()

        with open(filename) as f:
            for _split_line in f.readlines():
                _split_line = _split_line.split()
                words[_split_line[0]] = _split_line[1:]

        print(words['0'][200:235])
        print(words['2'][200:235])
        print(words['3'][200:235])
        print(words['5'][200:235])
        # print(words.keys())

        aco_model = AcoModelSet.load_aco_model_set('data/ph_models')

        print(aco_model.find_model('ay').trained_phoneme)
        print(aco_model.find_model('aa').trained_phoneme)
        print(aco_model.find_model('rr').trained_phoneme)
        print(aco_model.find_model('aa').trained_phoneme)

        distance = {}

        for _word, _phonems in words.items():

            assert len(_phonems) == len(ftr_vector)

            distance[_word] = []

            for i in range(len(_phonems)):
                distance[_word].append(aco_model.find_model(_phonems[i]).dist(ftr_vector[i]))

        return distance

#TEST

DEBUG = '/home/alex/projects/pycharm/ASRlesson050/debug'
TO_RECOGNIZE = '/home/alex/projects/pycharm/ASRlesson050/digits/digits_test.txtftr'

features_to_recognize = ArkReader()
features_to_recognize.read_file(TO_RECOGNIZE)

print(features_to_recognize.get_feature('0_4.wav')[200:235])

dist = PlotDistInFinalTokens().load(DEBUG + '/0_4.debug.txt', features_to_recognize.get_feature('0_4.wav'))

print('Distance: ')
print(dist)

print(sum(dist['0']))
print(sum(dist['2']))
print(sum(dist['3']))
print(sum(dist['5']))

plt.plot(dist['0'], 'b')
plt.plot(dist['2'], 'k')
plt.plot(dist['3'], 'r')
plt.plot(dist['5'], 'm')
# plt.plot(dist['5'], 'y')
plt.show()
