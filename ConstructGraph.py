
from collections import namedtuple
from collections import deque

from AcoModels import AcoModel
from AcoModels import AcoModelSet


class Graph:

    Node = namedtuple('Node', ['model',
                               'word',
                               'is_final',
                               'links_to_next_state'])

    Link = namedtuple('Link', ['state',
                               'inner_states'])

class ConstructGraph:

    @staticmethod
    def costruct(file_dic_name, aco_model_set, step=float('inf')):
        """  """

        ''' Create first node '''
        head = Graph.Node(model=None, word='', is_final=False, links_to_next_state=[])

        ''' Read new word from dic '''
        for _word in open(file_dic_name).readlines():
            if len(_word.strip()) > 0:
                ConstructGraph.create_word_nodes(_word, aco_model_set, head, step)

        return head

    @staticmethod
    def create_word_nodes(word_from_dic, aco_model_set, head, step):

        word, *phonemes = word_from_dic.strip().split()
        # print('word: ', word)
        # print('phonemes: ', phonemes)

        prev_node = head

        idx = 0
        jump_from_node = head
        inner_state = deque()

        for _ph in phonemes:

            if _ph.find('SIL') == -1 or prev_node == head:
                ''' Create a new node for non SIL phoneme or for the first SIL'''
                new_node = Graph.Node(model=aco_model_set.find_model(_ph),
                                      word='', is_final=False, links_to_next_state=[])
            else:
                ''' Final SIL has word and is_final=True '''
                new_node = Graph.Node(model=aco_model_set.find_model(_ph),
                                      word=word, is_final=True, links_to_next_state=[])

                ''' Set prev node as final too '''
                prev_node = prev_node._replace(word=word, is_final=True)

            ''' Add loop back '''
            new_node.links_to_next_state.append(Graph.Link(state=new_node, inner_states=None))

            ''' Set pointer to new node '''
            prev_node.links_to_next_state.append(Graph.Link(state=new_node, inner_states=None))

            ''' Add jump to node '''
            if idx > step:
                if jump_from_node != head:
                    jump_from_node.links_to_next_state.append(Graph.Link(state=new_node, inner_states=list(inner_state)))
                jump_from_node = inner_state.popleft()

            ''' Move to next node '''
            prev_node = new_node

            idx += 1
            inner_state.append(new_node)
            # print('inner_state : ', [_state.model.name for _state in inner_state])

    @staticmethod
    def print_graph(node):

        model_name = "None"
        if node.model:
            model_name = node.model.name

        print('-' * 30)
        print('name: {} state {} \nword: {}, is_final: {},\nnext_states {},\nnext_states {},\ninner_states {},\nftr {}'
              .format(model_name, id(node), node.word, node.is_final,
                      ConstructGraph.get_next_states_name(node),
                      [id(_state) for _state in ConstructGraph.get_next_states_name(node)],
                      [_state.inner_states for _state in node.links_to_next_state],
                      node.model.trained_phoneme if node.model else []))

        for _next_node in node.links_to_next_state:
            if node != _next_node.state:
                ConstructGraph.print_graph(_next_node.state)

    @staticmethod
    def get_next_states_name(node):
        return [_next_node.state.model.name for _next_node in node.links_to_next_state]

#TEST
#
# aco_model = AcoModelSet.load_aco_model_set('data/ph_models')
# head_node = ConstructGraph.costruct('data/ab.dic', aco_model, 2)
#
# print('\n' + '#' * 30 + '\n' + ' Debug '.center(30, '~') + '\n' + '#'*30)
# ConstructGraph.print_graph(head_node)