
from AcoModels import AcoModel
from AcoModels import AcoModelSet

from ArkReader import ArkReader
from Token import TokenPassing
from ConstructGraph import ConstructGraph


def recognition(phoneme_model_path, dict_path, to_recognize_data):
    """ """

    ''' Loading graph '''
    aco_model = AcoModelSet.load_aco_model_set(phoneme_model_path)
    head_node = ConstructGraph.costruct(dict_path, aco_model)

    ''' Create ark file reader '''
    features_to_recognize = ArkReader()

    ''' Read a file with features'''
    features_to_recognize.read_file(to_recognize_data)

    ''' Create and initialize TokenPassing object '''
    engine = TokenPassing(head_node)

    ''' Init vars to calculate an error '''
    patterns = 0
    errors = 0

    ''' Start recognizer for all files has read from "to_recognize_data" '''
    for _file in features_to_recognize.get_all_file_names():

        word, final_tokens = engine.run(features_to_recognize.get_feature(_file))
        patterns += 1

        if _file.split('_')[0].find(word) == -1:
            errors += 1

    return patterns, errors


def run_da_net(phoneme_model_path):

    dict_path = 'data/da_net/da_net.dic'
    to_recognize_data = 'data/da_net/da_net_test.txtftr'

    ''' Start recognition '''
    patterns, err = recognition(phoneme_model_path, dict_path, to_recognize_data)

    print('myDaNet : WER {:.2f}% [{}/{}]'.format(float(err)/patterns * 100.0, err, patterns))


def run_da_net_street(phoneme_model_path):

    dict_path = 'data/da_net/da_net.dic'
    to_recognize_data = 'data/da_net/street_da_net.txtftr'

    ''' Start recognition '''
    patterns, err = recognition(phoneme_model_path, dict_path, to_recognize_data)

    print('myStreetDaNet : WER {:.2f}% [{}/{}]'.format(float(err)/patterns * 100.0, err, patterns))


def run_digits(phoneme_model_path):

    dict_path = 'data/digits/digits.dic'
    to_recognize_data = 'data/digits/digits_test.txtftr'

    ''' Start recognition '''
    patterns, err = recognition(phoneme_model_path, dict_path, to_recognize_data)

    print('myDigits : WER {:.2f}% [{}/{}]'.format(float(err)/patterns * 100.0, err, patterns))


def run_digits_street(phoneme_model_path):
    dict_path = 'data/digits/digits.dic'
    to_recognize_data = 'data/digits/street_digits.txtftr'

    ''' Start recognition '''
    patterns, err = recognition(phoneme_model_path, dict_path, to_recognize_data)

    print('myStreetDigits : WER {:.2f}% [{}/{}]'.format(float(err) / patterns * 100.0, err, patterns))


def run_all_for_model(phoneme_model_path):

    print('\nModel : ', phoneme_model_path)

    run_da_net(phoneme_model_path)
    run_da_net_street(phoneme_model_path)
    run_digits(phoneme_model_path)
    run_digits_street(phoneme_model_path)


if __name__ == '__main__':

    phoneme_models = ('data/VoxForge/ph_models_def_2_sil_2'
                      , 'data/VoxForge/ph_models_def_4_sil_4'
                      , 'data/VoxForge/ph_models_def_8_sil_8'
                      , 'data/VoxForge/ph_models_def_10_sil_10'
                      , 'data/VoxForge/ph_models_def_10_sil_20'
                      , 'data/VoxForge/ph_models_def_12_sil_12'
                      , 'data/VoxForge/ph_models_def_12_sil_20'
                      , 'data/VoxForge/ph_models_def_20_sil_20'
                      , 'data/VoxForge/ph_models_def_20_sil_40')

    for _model in phoneme_models:
        run_all_for_model(_model)

    # # DEBUG = '/home/alex/projects/pycharm/ASRlesson050/debug'
    #
    # PHONE_DIR = '/home/alex/projects/pycharm/ASRlesson060/data/VoxForge/ph_models_def_20_sil_40'
    # # ALI_FILE = '/home/alex/lessons/ASR/decoder/test_data/data/VoxforgeRu/ali/monoali.phoneNames'
    # # ALI_FILE = '/home/alex/projects/pycharm/ASRlesson060/data/da_net/da_net.dic'
    # ALI_FILE = '/home/alex/projects/pycharm/ASRlesson060/data/digits/digits.dic'
    #
    # # TO_RECOGNIZE = '/home/alex/projects/pycharm/ASRlesson050/VoxForge/test_wav_features.txtftr'
    # # TO_RECOGNIZE = '/home/alex/projects/pycharm/ASRlesson060/data/da_net/da_net_etalons.txtftr'
    # # TO_RECOGNIZE = '/home/alex/projects/pycharm/ASRlesson060/data/da_net/da_net_test.txtftr'
    # # TO_RECOGNIZE = '/home/alex/projects/pycharm/ASRlesson060/data/da_net/street_da_net.txtftr'
    # # TO_RECOGNIZE = '/home/alex/projects/pycharm/ASRlesson060/data/digits/digits_etalons.txtftr'
    # # TO_RECOGNIZE = '/home/alex/projects/pycharm/ASRlesson060/data/digits/digits_test.txtftr'
    # TO_RECOGNIZE = '/home/alex/projects/pycharm/ASRlesson060/data/digits/street_digits.txtftr'
    #
    # print('Loading graph ...')
    # aco_model = AcoModelSet.load_aco_model_set(PHONE_DIR)
    # head_node = ConstructGraph.costruct(ALI_FILE, aco_model)
    #
    # # print('\n' + '#' * 30 + '\n' + ' Debug '.center(30, '~') + '\n' + '#'*30)
    # # ConstructGraph.print_graph(head_node)
    #
    # features_to_recognize = ArkReader()
    # features_to_recognize.read_file(TO_RECOGNIZE)
    #
    # engine = TokenPassing(head_node)
    #
    # patterns = 0
    # errors = 0
    #
    # for _file in features_to_recognize.get_all_file_names():
    #     print('\nRecognize File : ', _file)
    #     # print('-' * 30 + '\n' + _file.center(30, '~') + '\n' + '-' * 30)
    #     word, final_tokens = engine.run(features_to_recognize.get_feature(_file))
    #
    #     # if _file == '0_4.wav':
    #     #     TokenPassing.save_recognized_transcripts(final_tokens, DEBUG + '/0_4.debug.txt')
    #
    #     patterns += 1
    #
    #     if _file.split('_')[0].find(word) == -1:
    #         errors += 1
    #         print('ERROR')
    #
    # print('patterns : ', patterns)
    # print('errors : ', errors)

